$(".link-compra").click(function(e) {
    e.preventDefault();
    var aid = $(this).attr("href");
    $('html,body').animate({
        scrollTop: $(aid).offset().top
    }, 'slow');
});

var parametros = window.location.search;
var checkout = 'https://go.hotmart.com/W18336391S?ap=9eaa&src=GadsBTN01';

var btncheckout = checkout + parametros;

$(".btn-compra").attr('href', btncheckout);


$('.owl-carousel').owlCarousel({
    items: 1,
    merge: true,
    loop: true,
    margin: 10,
    video: true,
    center: true,
    responsive: {
        480: {
            items: 2
        },
        600: {
            items: 4
        }
    }
})


var zerouCronometro = localStorage.getItem('zerouCronometro');
var tempoCronometro = Number(localStorage.getItem('tempoCronometro')) || 0;
if (tempoCronometro == 0) {
    localStorage.setItem('tempoCronometro', 0)
}

// pega os elementos pelos ids.
//var dia = document.getElementById('dia')
//var hora = document.getElementById('hora')
var dia = $('.dia');
var hora = $('.hora');
var minuto = $('.minuto');
var segundo = $('.segundo');
var milesimo = $('.milesimo');

// crio das datas para o cronometro
var data = moment(tempoCronometro)
    .set('date', 1)
    .set('hour', 6)
    .set('minute', 00)
    .set('second', 00)
    .set('millisecond', 000)
    .subtract(tempoCronometro, 'ms');

var horaZero = moment(tempoCronometro)
    .set('date', 00)
    .set('hour', 00)
    .set('minute', 00)
    .set('second', 00)
    .set('millisecond', 000);

if (zerouCronometro) {
    dia.text('00');
    hora.text('00');
    minuto.text('00');
    segundo.text('00');
    milesimo.text('000');
} else {

    var cronometro = setInterval(function() {
        // se a data zerou, para tudo e seta o cookie
        if (data.unix() == horaZero.unix()) {

            milesimo.innerHTML = '000';
            paraCronometro();
        } else {

            // se ainda nao zerou, diminui 100ms
            data = data.subtract(100, 'ms');
            // dia.innerHTML = data.format('DD');
            // hora.innerHTML = data.format('HH');
            dia.text(data.format('DD'));
            hora.text(data.format('HH'));
            minuto.text(data.format('mm'));
            segundo.text(data.format('ss'));
            milesimo.text(data.format('SSS'));
            tempoCronometro = tempoCronometro + 100;
            localStorage.setItem('tempoCronometro', tempoCronometro);
        }
    }, 100);
}

function paraCronometro() {
    clearInterval(cronometro);
    localStorage.setItem('zerouCronometro', true);
}

var d = new Date();
var month = new Array();
month[0] = "Janeiro";
month[1] = "Fevereiro";
month[2] = "Março";
month[3] = "Abril";
month[4] = "Maio";
month[5] = "Junho";
month[6] = "Julho";
month[7] = "Agosto";
month[8] = "Setembro";
month[9] = "Outubro";
month[10] = "Novembro";
month[11] = "Dezembro";
var n = month[d.getMonth()];
dataHora = d.getDate() + ' de ' + n + ' de ' + d.getFullYear();
dataHoraFinal = d.getDate() + ' de ' + n + ' de ' + d.getFullYear();
dataHoraLateral = d.getDate() + ' de ' + n + ' de ' + d.getFullYear();
document.getElementById("datahoje").innerHTML = dataHora;


var relogio = document.getElementById('relogioReal');
setInterval(function() {
    relogio.innerHTML = ((new Date).toLocaleString().substr(11, 8));
}, 1000);


$(function() {
    var $doc = $('html, body');
    var url = '#compra-plano';

    $('.butao').attr('href', url);

    $('.butao').click(function() {
        $doc.animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 500);
        return false;
    });
});


// Função para aumentar o numero de clientes
function unitRandom(min, max) {
    return Math.floor(Math.random() * (max + min - 1)) + min;
}

function timeRandom(min, max) {
    return Math.floor(Math.random() * (max + min - 1)) + min;
}

$(function() {
    number = 10397;
    somar = unitRandom(1,3);
    setInterval(function(){ 
        number = number + somar;
        numberSave = number;
        number = number.toString();
        str = number;
        res1 = str.slice(0,2);
        res2 = str.slice(2,5);
        number = res1 + '.' + res2;
        $('.numeroAlunos').html(number);
        number = numberSave;
    }, timeRandom(3000, 5000));
});